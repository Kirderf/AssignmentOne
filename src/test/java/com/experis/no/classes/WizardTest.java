package com.experis.no.classes;

import com.experis.no.classes.attributes.HeroAttributes;
import com.experis.no.items.Equipmentslots;
import com.experis.no.items.armor.Armor;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WizardTest {
    public Hero wizard;
    @BeforeEach
    void setup(){
        wizard = new Wizard("Harry");
    }
    @Test
    void initWizardHero_validInit_shouldInitializeDefaultValues() {
        int levelExpected = 1;
        int strengthExpected = 1;
        int dexterityExpected = 1;
        int intelligenceExpected = 8;

        int levelActual = wizard.getLevel();
        int strengthActual = wizard.getStrength();
        int dexterityActual = wizard.getDexterity();
        int intelligenceActual = wizard.getIntelligence();

        assertEquals(levelExpected, levelActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }
    @Test
    void levelUp_validMethod_shouldReturnLevel() {

        int levelExpected = 2;
        int strengthExpected = 1 + 1;
        int dexterityExpected = 1 + 1;
        int intelligenceExpected = 8 + 5;

        wizard.levelUp();

        int levelActual = wizard.getLevel();
        int strengthActual = wizard.getStrength();
        int dexterityActual = wizard.getDexterity();
        int intelligenceActual = wizard.getIntelligence();

        assertEquals(levelExpected, levelActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);

    }

    @Test
    void calculateWizardDamageNoWeapon_validCalc_shouldReturnDamage(){
        double expected = 1.0;
        double actual = wizard.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateWizardDamageWeapon_validCalc_shouldReturnDamage(){

        double expected = 4 * (1 + (double) 8 / 100);

        Weapon wand = new Weapon("wand of fire",1, WeaponType.Wand,4);
        wizard.equip(Equipmentslots.Weapon,wand);

        double actual = wizard.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateWizardDamageWeaponReplaceWeapon_validCalc_shouldReturnDamage(){

        double expected = 10 * (1 + (double) 8 / 100);

        Weapon wand = new Weapon("wand of fire",1, WeaponType.Wand,4);
        wizard.equip(Equipmentslots.Weapon,wand);
        Weapon staff = new Weapon("Staff of air",1,WeaponType.Staff,10);
        wizard.equip(Equipmentslots.Weapon,staff);

        double actual = wizard.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateWizardDamageWeaponWithArmor_validCalc_shouldReturnDamage(){

        double expected = 4 * (1 + (double) 28 / 100);

        Weapon wand = new Weapon("wand of fire",1, WeaponType.Wand,4);
        Armor hat = new Armor("Wizards hat",1,Equipmentslots.Head, ArmorType.Cloth,new HeroAttributes(0,0,20));
        wizard.equip(Equipmentslots.Weapon,wand);
        wizard.equip(Equipmentslots.Head,hat);

        double actual = wizard.calculateHeroDamage();

        assertEquals(expected,actual);
    }

}