package com.experis.no.classes;

import com.experis.no.classes.attributes.HeroAttributes;
import com.experis.no.items.Equipmentslots;
import com.experis.no.items.armor.Armor;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SwashbucklerTest {
    public Hero swashb;

    @BeforeEach
    void setup() {
        swashb = new Swashbuckler("Per the Pirate");
    }

    @Test
    void initSwashbucklerHero_validInit_shouldInitializeDefaultValues() {
        int levelExpected = 1;
        int strengthExpected = 2;
        int dexterityExpected = 6;
        int intelligenceExpected = 1;

        int levelActual = swashb.getLevel();
        int strengthActual = swashb.getStrength();
        int dexterityActual = swashb.getDexterity();
        int intelligenceActual = swashb.getIntelligence();

        assertEquals(levelExpected, levelActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }

    @Test
    void levelUp_validMethod_shouldReturnLevel() {

        int levelExpected = 2;
        int strengthExpected = 2 + 1;
        int dexterityExpected = 6 + 4;
        int intelligenceExpected = 1 + 1;

        swashb.levelUp();

        int levelActual = swashb.getLevel();
        int strengthActual = swashb.getStrength();
        int dexterityActual = swashb.getDexterity();
        int intelligenceActual = swashb.getIntelligence();


        assertEquals(levelExpected, levelActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);

    }

    @Test
    void calculateSwashbucklerDamageNoWeapon_validCalc_shouldReturnDamage() {
        double expected = 1.0;
        double actual = swashb.calculateHeroDamage();

        assertEquals(expected, actual);
    }
    @Test
    void calculateSwashbucklerDamageWeapon_validCalc_shouldReturnDamage(){

        double expected = 4 * (1 + (double) 6 / 100);

        Weapon we = new Weapon("Rapier",1, WeaponType.Sword,4);
        swashb.equip(Equipmentslots.Weapon,we);

        double actual = swashb.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateSwashbucklerDamageWeaponReplaceWeapon_validCalc_shouldReturnDamage(){

        double expected = 24 * (1 + (double) 6 / 100);

        Weapon we = new Weapon("Rapier",1, WeaponType.Sword,4);
        swashb.equip(Equipmentslots.Weapon,we);
        Weapon we2 = new Weapon("Diamond Rapier",1, WeaponType.Sword,24);
        swashb.equip(Equipmentslots.Weapon,we2);

        double actual = swashb.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateSwashbucklerDamageWeaponWithArmor_validCalc_shouldReturnDamage(){

        double expected = 4 * (1 + (double) 26 / 100);

        Weapon we = new Weapon("Rapier",1, WeaponType.Sword,4);
        Armor hat = new Armor("Pointy hat",1,Equipmentslots.Head, ArmorType.Leather,new HeroAttributes(0,20,0));
        swashb.equip(Equipmentslots.Weapon,we);
        swashb.equip(Equipmentslots.Head,hat);

        double actual = swashb.calculateHeroDamage();

        assertEquals(expected,actual);
    }
}