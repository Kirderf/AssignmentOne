package com.experis.no.classes;

import com.experis.no.classes.attributes.HeroAttributes;
import com.experis.no.items.Equipmentslots;
import com.experis.no.items.Item;
import com.experis.no.items.armor.Armor;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    Hero wizard;
    @BeforeEach
    void setUp() {
        wizard = new Wizard("Harry");
    }

    @Test
    void getName_validMethod_shouldReturnName() {

        String expected = "Harry";
        String actual = wizard.getName();
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_validInput_shouldReturnTrue(){
        Weapon staff = new Weapon("Staff of water",1, WeaponType.Staff,10);
        assertTrue(wizard.equip(Equipmentslots.Weapon,staff));
    }
    @Test
    void equipWeapon_InvalidType_shouldReturnFalse(){
        Weapon commonHatchet = new Weapon("Common Hatchet",1, WeaponType.Hatchet,10);
        assertFalse(wizard.equip(Equipmentslots.Weapon,commonHatchet));
    }
    @Test
    void equipWeapon_InvalidLevel_shouldReturnFalse(){
        Weapon commonHatchet = new Weapon("Common Staff",2, WeaponType.Staff,1);
        assertFalse(wizard.equip(Equipmentslots.Weapon,commonHatchet));
    }
    @Test
    void equipWeapon_InvalidItem_shouldReturnFalse(){
        Item commonHatchet = new Item("Sticks",1,Equipmentslots.Inventory);
        assertFalse(wizard.equip(Equipmentslots.Weapon,commonHatchet));
    }

    @Test
    void equipArmor_validInput_shouldReturnTrue(){
        Armor commonChestPlate = new Armor("Common chestPlate",1, Equipmentslots.Body,ArmorType.Cloth,new HeroAttributes(1,0,0));
        assertTrue(wizard.equip(Equipmentslots.Body,commonChestPlate));
    }
    @Test
    void equipArmor_InvalidType_shouldReturnFalse(){
        Armor commonChestPlate = new Armor("Common chestPlate",1, Equipmentslots.Body,ArmorType.Mail,new HeroAttributes(1,0,0));
        assertFalse(wizard.equip(Equipmentslots.Weapon,commonChestPlate));
    }
    @Test
    void equipArmor_InvalidLevel_shouldReturnFalse(){
        Armor commonChestPlate = new Armor("Common chestPlate",3, Equipmentslots.Body,ArmorType.Cloth,new HeroAttributes(1,0,0));
        assertFalse(wizard.equip(Equipmentslots.Weapon,commonChestPlate));
    }
    @Test
    void equipArmorSet_validInput_shouldReturnTrue(){
        Armor commonChestPlate = new Armor("Common chestPlate",1, Equipmentslots.Body,ArmorType.Cloth,new HeroAttributes(1,0,0));
        Armor commonHelmet = new Armor("Wizard hat",1,Equipmentslots.Head,ArmorType.Cloth,new HeroAttributes(0,0,1));
        Armor commonLegs = new Armor("Robe leggings",1,Equipmentslots.Legs,ArmorType.Cloth,new HeroAttributes(0,1,0));

        assertTrue(wizard.equip(Equipmentslots.Head,commonHelmet));
        assertTrue(wizard.equip(Equipmentslots.Body,commonChestPlate));
        assertTrue(wizard.equip(Equipmentslots.Legs,commonLegs));
    }
    @Test
    void totalAttributes_validInput_shouldReturnHeroattributes(){
        HeroAttributes expected = new HeroAttributes(2,2,9);

        Armor commonChestPlate = new Armor("Common chestPlate",1, Equipmentslots.Body,ArmorType.Cloth,new HeroAttributes(1,0,0));
        Armor commonHelmet = new Armor("Wizard hat",1,Equipmentslots.Head,ArmorType.Cloth,new HeroAttributes(0,0,1));
        Armor commonLegs = new Armor("Robe leggings",1,Equipmentslots.Legs,ArmorType.Cloth,new HeroAttributes(0,1,0));

        wizard.equip(Equipmentslots.Head,commonHelmet);
        wizard.equip(Equipmentslots.Body,commonChestPlate);
        wizard.equip(Equipmentslots.Legs,commonLegs);
        assertEquals(expected.toString(), wizard.calculateTotalAttributes().toString());

    }
    @Test
    void totalAttributesReplaceArmor_validInput_shouldReturnHeroattributes(){
        HeroAttributes expected = new HeroAttributes(2,1,8);
        HeroAttributes expected2 = new HeroAttributes(5,1,8);

        Armor commonChestPlate = new Armor("Common chestPlate",1, Equipmentslots.Body,ArmorType.Cloth,new HeroAttributes(1,0,0));
        Armor commonChestPlate2 = new Armor("Common chestPlate",1, Equipmentslots.Body,ArmorType.Cloth,new HeroAttributes(4,0,0));

        wizard.equip(Equipmentslots.Head,commonChestPlate);
        assertEquals(expected.toString(), wizard.calculateTotalAttributes().toString());
        wizard.equip(Equipmentslots.Head,commonChestPlate2);
        assertEquals(expected2.toString(), wizard.calculateTotalAttributes().toString());
    }
    @Test
    void displayHero_validInput_shouldDisplayHero(){
        String expected = """
                Name : Harry
                Class : Wizard
                Level : 1
                Total Strength : 1
                Total Dexterity : 1
                Total Intelligence : 8
                Damage : 1.0
                """;
        assertEquals(expected,wizard.display());
    }
}