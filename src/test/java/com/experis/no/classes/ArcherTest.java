package com.experis.no.classes;

import com.experis.no.classes.attributes.HeroAttributes;
import com.experis.no.items.Equipmentslots;
import com.experis.no.items.armor.Armor;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArcherTest {
    public Hero archer;

    @BeforeEach
    void setup() {
        archer = new Archer("Robin Hood");
    }

    @Test
    void initArcherHero_validInit_shouldInitializeDefaultValues() {
        int levelExpected = 1;
        int strengthExpected = 1;
        int dexterityExpected = 7;
        int intelligenceExpected = 1;

        int levelActual = archer.getLevel();
        int strengthActual = archer.getStrength();
        int dexterityActual = archer.getDexterity();
        int intelligenceActual = archer.getIntelligence();

        assertEquals(levelExpected, levelActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }
    @Test
    void levelUp_validMethod_shouldReturnLevel() {

        int levelExpected = 2;
        int strengthExpected = 1 + 1;
        int dexterityExpected = 7 + 5;
        int intelligenceExpected = 1 + 1;

        archer.levelUp();

        int levelActual = archer.getLevel();
        int strengthActual = archer.getStrength();
        int dexterityActual = archer.getDexterity();
        int intelligenceActual = archer.getIntelligence();

        assertEquals(levelExpected, levelActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);

    }
    @Test
    void calculateArcherDamageNoWeapon_validCalc_shouldReturnDamage(){
        double expected = 1.0;
        double actual = archer.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateArcherDamageWeapon_validCalc_shouldReturnDamage(){

        double expected = 4 * (1 + (double) 7 / 100);

        Weapon we = new Weapon("Common bow",1, WeaponType.Bow,4);
        archer.equip(Equipmentslots.Weapon,we);

        double actual = archer.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateArcherDamageWeaponReplaceWeapon_validCalc_shouldReturnDamage(){

        double expected = 400 * (1 + (double) 7 / 100);

        Weapon we = new Weapon("Common bow",1, WeaponType.Bow,4);
        archer.equip(Equipmentslots.Weapon,we);
        Weapon we2 = new Weapon("GOD bow",1, WeaponType.Bow,400);
        archer.equip(Equipmentslots.Weapon,we2);

        double actual = archer.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateArcherDamageWeaponWithArmor_validCalc_shouldReturnDamage(){

        double expected = 4 * (1 + (double) 27 / 100);

        Weapon we = new Weapon("Common bow",1, WeaponType.Bow,4);
        Armor hat = new Armor("Hood",1,Equipmentslots.Head, ArmorType.Leather,new HeroAttributes(0,20,0));
        archer.equip(Equipmentslots.Weapon,we);
        archer.equip(Equipmentslots.Head,hat);

        double actual = archer.calculateHeroDamage();

        assertEquals(expected,actual);
    }
}