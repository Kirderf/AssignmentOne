package com.experis.no.classes;

import com.experis.no.classes.attributes.HeroAttributes;
import com.experis.no.items.Equipmentslots;
import com.experis.no.items.armor.Armor;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BarbarianTest {

    public Hero barb;

    @BeforeEach
    void setup() {
        barb = new Barbarian("Ragnar The Viking");
    }
    @Test
    void initBarbarianHero_validInit_shouldInitializeDefaultValues() {
        int levelExpected = 1;
        int strengthExpected = 5;
        int dexterityExpected = 2;
        int intelligenceExpected = 1;

        int levelActual = barb.getLevel();
        int strengthActual = barb.getStrength();
        int dexterityActual = barb.getDexterity();
        int intelligenceActual = barb.getIntelligence();

        assertEquals(levelExpected, levelActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }
    @Test
    void levelUp_validMethod_shouldReturnLevel() {
        int levelExpected = 2;
        int strengthExpected = 5 + 3;
        int dexterityExpected = 2 + 2;
        int intelligenceExpected = 1 + 1;

        barb.levelUp();

        int levelActual = barb.getLevel();
        int strengthActual = barb.getStrength();
        int dexterityActual = barb.getDexterity();
        int intelligenceActual = barb.getIntelligence();

        assertEquals(levelExpected, levelActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);

    }

    @Test
    void calculateBarbarianDamageNoWeapon_validCalc_shouldReturnDamage(){
        double expected = 1.0;
        double actual = barb.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateBarbarianDamageWeapon_validCalc_shouldReturnDamage(){

        double expected = 42 * (1 + (double) 5 / 100);

        Weapon we = new Weapon("BattleAxe",1, WeaponType.Hatchet,42);
        barb.equip(Equipmentslots.Weapon,we);

        double actual = barb.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateBarbarianDamageWeaponReplaceWeapon_validCalc_shouldReturnDamage(){

        double expected = 24 * (1 + (double) 5 / 100);

        Weapon we = new Weapon("BattleAxe",1, WeaponType.Hatchet,42);
        barb.equip(Equipmentslots.Weapon,we);
        Weapon we2 = new Weapon("Wooden axe",1, WeaponType.Hatchet,24);
        barb.equip(Equipmentslots.Weapon,we2);

        double actual = barb.calculateHeroDamage();

        assertEquals(expected,actual);
    }
    @Test
    void calculateBarbarianDamageWeaponWithArmor_validCalc_shouldReturnDamage(){

        double expected = 42 * (1 + (double) 12 / 100);

        Weapon we = new Weapon("BattleAxe",1, WeaponType.Hatchet,42);
        Armor hat = new Armor("Mail helmet",1,Equipmentslots.Head, ArmorType.Mail,new HeroAttributes(7,0,0));
        barb.equip(Equipmentslots.Weapon,we);
        barb.equip(Equipmentslots.Head,hat);

        double actual = barb.calculateHeroDamage();

        assertEquals(expected,actual);
    }
}