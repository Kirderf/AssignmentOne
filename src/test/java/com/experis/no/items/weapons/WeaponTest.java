package com.experis.no.items.weapons;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    Weapon weapon;
    @BeforeEach
    void genWeapon(){
        weapon = new Weapon("Common Hatchet",1,WeaponType.Hatchet,2);
    }
    @Test
    void isCorrectWeaponName() {
        String expected = "Common Hatchet";
        String actual = weapon.getName();
        assertEquals(expected,actual);
    }
    @Test
    void isCorrectWeaponLevel() {
        int expected = 1;
        int actual = weapon.getRequiredLevel();
        assertEquals(expected,actual);
    }

    @Test
    void isCorrectWeaponType(){
        WeaponType expectedType = WeaponType.Hatchet;
        WeaponType actualType = weapon.getType();
        assertEquals(expectedType, actualType);
    }
    @Test
    void isCorrectWeaponDamage(){
        double expectedDamage = 2;
        double actualDamage = weapon.getWeaponDamage();
        assertEquals(expectedDamage, actualDamage);
    }

    @Test
    void getType() {
    }
}