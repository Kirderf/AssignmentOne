package com.experis.no.items.armor;

import com.experis.no.classes.attributes.HeroAttributes;
import com.experis.no.items.Equipmentslots;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArmorTest {
    Armor armor;
    @BeforeEach
    void setUp() {
        armor = new Armor("Common Plate Chest",1, Equipmentslots.Body,ArmorType.Plate,new HeroAttributes(1,2,3));
    }
    @Test
    void isCorrectArmorName() {
        String expected = "Common Plate Chest";
        String actual = armor.getName();
        assertEquals(expected,actual);
    }
    @Test
    void isCorrectArmorLevel() {
        int expected = 1;
        int actual = armor.getRequiredLevel();
        assertEquals(expected,actual);
    }

    @Test
    void isCorrectArmorType(){
        ArmorType expectedType = ArmorType.Plate;
        ArmorType actualType = armor.getArmorType();
        assertEquals(expectedType, actualType);
    }
    @Test
    void isCorrectArmorIncrease(){
        int expectedIncreaseStr = 1;
        int expectedIncreaseDex = 2;
        int expectedIncreaseInt = 3;
        HeroAttributes expected = new HeroAttributes(expectedIncreaseStr,expectedIncreaseDex,expectedIncreaseInt);
        HeroAttributes actualIncrease = armor.getArmorAttribute();
        assertEquals(expected.getStrength(), actualIncrease.getStrength());
        assertEquals(expected.getDexterity(), actualIncrease.getDexterity());
        assertEquals(expected.getIntelligence(), actualIncrease.getIntelligence());
    }
    @Test
    void getArmorType() {
    }

    @Test
    void getArmorAttribute() {
    }
}