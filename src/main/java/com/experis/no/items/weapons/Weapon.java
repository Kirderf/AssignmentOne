package com.experis.no.items.weapons;

import com.experis.no.items.Equipmentslots;
import com.experis.no.items.Item;

/**
 * The type Weapon.
 */
public class Weapon extends Item {
    private final int weaponDamage;
    private final WeaponType type;

    /**
     * Instantiates a new Weapon.
     *
     * @param name          the name
     * @param requiredLevel the required level
     * @param type          the type
     * @param weaponDamage  the weapon damage
     */
    public Weapon(String name, int requiredLevel, WeaponType type, int weaponDamage) {
        super(name, requiredLevel, Equipmentslots.Weapon);
        this.type = type;
        this.weaponDamage = weaponDamage;
    }

    /**
     * Gets weapon damage.
     *
     * @return the weapon damage
     */
    public int getWeaponDamage() {
        return weaponDamage;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public WeaponType getType() {
        return type;
    }

}
