package com.experis.no.items.weapons;

/**
 * The enum Weapon type.
 */
public enum WeaponType {
    /**
     * Hatchet weapon type.
     */
    Hatchet,
    /**
     * Bow weapon type.
     */
    Bow,
    /**
     * Dagger weapon type.
     */
    Dagger,
    /**
     * Mace weapon type.
     */
    Mace,
    /**
     * Staff weapon type.
     */
    Staff,
    /**
     * Sword weapon type.
     */
    Sword,
    /**
     * Wand weapon type.
     */
    Wand,
}
