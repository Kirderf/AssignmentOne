package com.experis.no.items.exceptions;

/**
 * The type Invalid armor exception.
 */
public class InvalidArmorException extends Exception {
    /**
     * Instantiates a new Invalid armor exception.
     *
     * @param message the message
     */
    public InvalidArmorException(String message) {
        super(message);
    }
}
