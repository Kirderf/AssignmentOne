package com.experis.no.items.exceptions;

/**
 * The type Invalid weapon exception.
 */
public class InvalidWeaponException extends Exception {
    /**
     * Instantiates a new Invalid weapon exception.
     *
     * @param message the message
     */
    public InvalidWeaponException(String message) {
        super(message);
    }
}
