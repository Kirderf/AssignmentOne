package com.experis.no.items.armor;

import com.experis.no.classes.attributes.HeroAttributes;
import com.experis.no.items.Equipmentslots;
import com.experis.no.items.Item;

/**
 * The type Armor.
 */
public class Armor extends Item {
    private final ArmorType armorType;
    private final HeroAttributes armorAttribute;

    /**
     * Instantiates a new Armor.
     *
     * @param name           the name
     * @param requiredLevel  the required level
     * @param equipmentslots the equipmentslots
     * @param armorType      the armor type
     * @param armorAttribute the armor attribute
     */
    public Armor(String name, int requiredLevel, Equipmentslots equipmentslots, ArmorType armorType, HeroAttributes armorAttribute) {
        super(name, requiredLevel, equipmentslots);
        this.armorType = armorType;
        this.armorAttribute = armorAttribute;
    }

    /**
     * Gets armor type.
     *
     * @return the armor type
     */
    public ArmorType getArmorType() {
        return armorType;
    }

    /**
     * Gets armor attribute.
     *
     * @return the armor attribute
     */
    public HeroAttributes getArmorAttribute() {
        return armorAttribute;
    }
}
