package com.experis.no.items.armor;

/**
 * The enum Armor type.
 */
public enum ArmorType {
    /**
     * Cloth armor type.
     */
    Cloth,
    /**
     * Leather armor type.
     */
    Leather,
    /**
     * Mail armor type.
     */
    Mail,
    /**
     * Plate armor type.
     */
    Plate
}
