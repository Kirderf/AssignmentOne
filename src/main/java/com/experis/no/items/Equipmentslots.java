package com.experis.no.items;

/**
 * The enum Equipmentslots.
 */
public enum Equipmentslots {
    /**
     * Weapon equipmentslots.
     */
    Weapon,
    /**
     * Head equipmentslots.
     */
    Head,
    /**
     * Body equipmentslots.
     */
    Body,
    /**
     * Legs equipmentslots.
     */
    Legs,
    /**
     * Inventory equipmentslots.
     */
    Inventory
}
