package com.experis.no.items;

/**
 * The type Item.
 */
public class Item {
    /**
     * The Name.
     */
    String name;
    /**
     * The Required level.
     */
    int requiredLevel;
    /**
     * The Slot.
     */
    Equipmentslots slot;

    /**
     * Instantiates a new Item.
     *
     * @param name          the name
     * @param requiredLevel the required level
     * @param slot          the slot
     */
    public Item(String name, int requiredLevel, Equipmentslots slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets required level.
     *
     * @return the required level
     */
    public int getRequiredLevel() {
        return requiredLevel;
    }

}
