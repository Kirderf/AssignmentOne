package com.experis.no.classes;

import com.experis.no.items.Equipmentslots;
import com.experis.no.items.Item;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;

/**
 * The type Barbarian.
 */
public class Barbarian extends Hero {
    /**
     * Instantiates a new Barbarian.
     *
     * @param name the name
     */
    public Barbarian(String name) {
        super(name, 5, 2, 1);
        this.equipableWeapons.add(WeaponType.Hatchet);
        this.equipableWeapons.add(WeaponType.Mace);
        this.equipableWeapons.add(WeaponType.Sword);
        this.equipableArmor.add(ArmorType.Mail);
        this.equipableArmor.add(ArmorType.Plate);
    }

    @Override
    public void levelUp() {
        this.increaseLevel();
        this.increaseStrength(3);
        this.increaseDexterity(2);
        this.increaseIntelligence(1);
    }

    @Override
    public double calculateHeroDamage() {
        double damage = 1;
        Item item = inventory.get(Equipmentslots.Weapon);
        if (item instanceof Weapon) {
            damage = ((Weapon) item).getWeaponDamage() * (1 + (double) calculateTotalAttributes().getStrength() / 100);
        }
        return damage;
    }
}
