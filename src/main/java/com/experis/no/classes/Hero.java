package com.experis.no.classes;

import com.experis.no.classes.attributes.HeroAttributes;
import com.experis.no.items.Equipmentslots;
import com.experis.no.items.Item;
import com.experis.no.items.armor.Armor;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.exceptions.InvalidArmorException;
import com.experis.no.items.exceptions.InvalidWeaponException;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The type Hero.
 */
public abstract class Hero extends HeroAttributes {
    private final String Name;
    private int level;

    /**
     * The Equipable weapons.
     */
    protected ArrayList<WeaponType> equipableWeapons = new ArrayList<>();
    /**
     * The Equipable armor.
     */
    protected ArrayList<ArmorType> equipableArmor = new ArrayList<>();
    /**
     * The Inventory.
     */
    protected HashMap<Equipmentslots, Item> inventory = new HashMap<>();

    /**
     * Instantiates a new Hero.
     *
     * @param name         the name
     * @param strength     the strength
     * @param dexterity    the dexterity
     * @param intelligence the intelligence
     */
    public Hero(String name, int strength, int dexterity, int intelligence) {
        super(strength, dexterity, intelligence);
        Name = name;
        this.level = 1;
        inventory.put(Equipmentslots.Weapon, null);
        inventory.put(Equipmentslots.Head, null);
        inventory.put(Equipmentslots.Body, null);
        inventory.put(Equipmentslots.Legs, null);

    }

    /**
     * Level up.
     */
    protected abstract void levelUp();

    /**
     * Calculate hero damage double.
     *
     * @return the double
     */
    protected abstract double calculateHeroDamage();

    /**
     * Equip boolean.
     *
     * @param equipmentslots the equipmentslots
     * @param item           the item
     * @return the boolean
     */
    public boolean equip(Equipmentslots equipmentslots, Item item) {
        try {
            if (item instanceof Weapon) {
                if (!equipableWeapons.contains(((Weapon) item).getType())) {
                    throw new InvalidWeaponException("Cannot equip this weaponType!");
                }
            } else if (item instanceof Armor) {
                if (!equipableArmor.contains(((Armor) item).getArmorType())) {
                    throw new InvalidArmorException("Cannot equip this armorType");
                }
            } else {
                throw new Exception("Not an equipable item");
            }
            if (this.level >= item.getRequiredLevel()) {
                inventory.put(equipmentslots, item);
                return true;
            } else {
                throw new Exception("Too high of a level requirement");
            }

        } catch (InvalidWeaponException | InvalidArmorException exception) {
            System.out.println(exception.getMessage());
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }


    /**
     * Calculate total attributes hero attributes.
     *
     * @return the hero attributes
     */
    protected HeroAttributes calculateTotalAttributes() {
        int dex = this.getDexterity();
        int str = this.getStrength();
        int inte = this.getIntelligence();
        for (Equipmentslots slot : inventory.keySet()) {
            if (slot.ordinal() > 0 && slot.ordinal() < 4) {
                Item item = inventory.get(slot);
                if (item instanceof Armor) {
                    HeroAttributes armorAttribute = ((Armor) item).getArmorAttribute();
                    dex += armorAttribute.getDexterity();
                    str += armorAttribute.getStrength();
                    inte += armorAttribute.getIntelligence();
                }
            }
        }

        return new HeroAttributes(str, dex, inte);
    }

    /**
     * Display string.
     *
     * @return the string
     */
    public String display() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name : ").append(this.Name).append("\n");
        sb.append("Class : ").append(this.getClass().getSimpleName()).append("\n");
        sb.append("Level : ").append(this.level).append("\n");
        HeroAttributes heroAttributes = this.calculateTotalAttributes();
        sb.append("Total Strength : ").append(heroAttributes.getStrength()).append("\n");
        sb.append("Total Dexterity : ").append(heroAttributes.getDexterity()).append("\n");
        sb.append("Total Intelligence : ").append(heroAttributes.getIntelligence()).append("\n");
        sb.append("Damage : ").append(calculateHeroDamage()).append("\n");
        System.out.println(sb);
        return sb.toString();
    }


    /**
     * Increase level.
     */
    public void increaseLevel() {
        this.level++;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return Name;
    }

    /**
     * Gets level.
     *
     * @return the level
     */
    public int getLevel() {
        return level;
    }

}
