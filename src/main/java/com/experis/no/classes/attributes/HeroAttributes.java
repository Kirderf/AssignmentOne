package com.experis.no.classes.attributes;

import java.util.Objects;

/**
 * The type Hero attributes.
 */
public class HeroAttributes {
    private int dexterity;
    private int strength;
    private int intelligence;

    /**
     * Instantiates a new Hero attributes.
     *
     * @param strength     the strength
     * @param dexterity    the dexterity
     * @param intelligence the intelligence
     */
    public HeroAttributes(int strength, int dexterity, int intelligence) {
        this.dexterity = dexterity;
        this.strength = strength;
        this.intelligence = intelligence;
    }

    /**
     * Increase dexterity.
     *
     * @param amount the amount
     */
    protected void increaseDexterity(int amount) {
        dexterity += amount;
    }

    /**
     * Increase strength.
     *
     * @param amount the amount
     */
    protected void increaseStrength(int amount) {
        strength += amount;
    }

    /**
     * Increase intelligence.
     *
     * @param amount the amount
     */
    protected void increaseIntelligence(int amount) {
        intelligence += amount;
    }

    /**
     * Gets dexterity.
     *
     * @return the dexterity
     */
    public int getDexterity() {
        return dexterity;
    }

    /**
     * Gets strength.
     *
     * @return the strength
     */
    public int getStrength() {
        return strength;
    }

    /**
     * Gets intelligence.
     *
     * @return the intelligence
     */
    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeroAttributes that = (HeroAttributes) o;
        return dexterity == that.dexterity && strength == that.strength && intelligence == that.intelligence;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dexterity, strength, intelligence);
    }

    @Override
    public String toString() {
        return "HeroAttributes{" +
                "dexterity=" + dexterity +
                ", strength=" + strength +
                ", intelligence=" + intelligence +
                '}';
    }

}
