package com.experis.no.classes;

import com.experis.no.items.Equipmentslots;
import com.experis.no.items.Item;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;

/**
 * The type Swashbuckler.
 */
public class Swashbuckler extends Hero {
    /**
     * Instantiates a new Swashbuckler.
     *
     * @param name the name
     */
    public Swashbuckler(String name) {
        super(name, 2, 6, 1);
        this.equipableWeapons.add(WeaponType.Dagger);
        this.equipableWeapons.add(WeaponType.Sword);
        this.equipableArmor.add(ArmorType.Leather);
        this.equipableArmor.add(ArmorType.Mail);
    }

    @Override
    public void levelUp() {
        this.increaseLevel();
        this.increaseStrength(1);
        this.increaseDexterity(4);
        this.increaseIntelligence(1);
    }

    @Override
    public double calculateHeroDamage() {
        double damage = 1;
        Item item = inventory.get(Equipmentslots.Weapon);
        if (item instanceof Weapon) {
            damage = ((Weapon) item).getWeaponDamage() * (1 + (double) calculateTotalAttributes().getDexterity() / 100);
        }
        return damage;
    }
}
