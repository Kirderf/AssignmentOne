package com.experis.no.classes;

import com.experis.no.items.Equipmentslots;
import com.experis.no.items.Item;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;

/**
 * The type Archer.
 */
public class Archer extends Hero {
    /**
     * Instantiates a new Archer.
     *
     * @param name the name
     */
    public Archer(String name) {
        super(name, 1, 7, 1);
        this.equipableWeapons.add(WeaponType.Bow);
        this.equipableArmor.add(ArmorType.Leather);
        this.equipableArmor.add(ArmorType.Mail);
    }

    @Override
    public void levelUp() {
        this.increaseLevel();
        this.increaseStrength(1);
        this.increaseDexterity(5);
        this.increaseIntelligence(1);
    }

    @Override
    public double calculateHeroDamage() {
        double damage = 1;
        Item item = inventory.get(Equipmentslots.Weapon);
        if (item instanceof Weapon) {
            damage = ((Weapon) item).getWeaponDamage() * (1 + (double) calculateTotalAttributes().getDexterity() / 100);
        }
        return damage;
    }
}
