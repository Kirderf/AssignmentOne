package com.experis.no.classes;

import com.experis.no.items.Equipmentslots;
import com.experis.no.items.Item;
import com.experis.no.items.armor.ArmorType;
import com.experis.no.items.weapons.Weapon;
import com.experis.no.items.weapons.WeaponType;

/**
 * The type Wizard.
 */
public class Wizard extends Hero {
    /**
     * Instantiates a new Wizard.
     *
     * @param name the name
     */
    public Wizard(String name) {
        super(name, 1, 1, 8);
        this.equipableWeapons.add(WeaponType.Staff);
        this.equipableWeapons.add(WeaponType.Wand);
        this.equipableArmor.add(ArmorType.Cloth);
    }

    @Override
    public void levelUp() {
        this.increaseLevel();
        this.increaseStrength(1);
        this.increaseDexterity(1);
        this.increaseIntelligence(5);
    }

    @Override
    public double calculateHeroDamage() {
        double damage = 1;
        Item item = inventory.get(Equipmentslots.Weapon);
        if (item instanceof Weapon) {
            damage = ((Weapon) item).getWeaponDamage() * (1 + (double) calculateTotalAttributes().getIntelligence() / 100);
        }
        return damage;
    }
}
